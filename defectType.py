# -*- coding:utf-8 -*-
# Author: maoge
# DATE  :2020/12/21
# TIME  :上午11:37
import os
import shutil

missPath = "/Users/maoge/DataLocal/1221.1/1221.1/true"
# missPath = "/Users/maoge/DataLocal/1221.1/1221.1/严重漏失"
# missPath = "/Volumes/厂商缺陷数据汇总/PengDing-Shenzhen/缺陷汇总-直报统计 - 副本"

for file in os.listdir(missPath):
    if os.path.isdir(file):
        pass
    defect_p = file.split(")")[0]
    type_path = os.path.join(missPath, "{}".format(defect_p))
    if not os.path.exists(type_path):
        os.mkdir(type_path)
    shutil.move(os.path.join(missPath, file), type_path)

print(len(os.listdir(missPath)))