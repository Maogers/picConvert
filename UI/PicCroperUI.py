# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PicCroperUI.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(497, 346)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(60, 84, 91, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(60, 150, 361, 16))
        self.label_2.setObjectName("label_2")
        self.lineEdit_scrImg = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_scrImg.setGeometry(QtCore.QRect(160, 80, 261, 21))
        self.lineEdit_scrImg.setObjectName("lineEdit_scrImg")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(40, 21, 381, 20))
        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        self.label_3.setFont(font)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.btn_crop = QtWidgets.QPushButton(self.centralwidget)
        self.btn_crop.setGeometry(QtCore.QRect(200, 210, 113, 32))
        self.btn_crop.setObjectName("btn_crop")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 497, 24))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "输入原图路径"))
        self.label_2.setText(_translate("MainWindow", "输出图片路径为原图路径同级，后缀为_croped文件夹"))
        self.label_3.setText(_translate("MainWindow", "功能：将大余300的图片裁剪成300x300"))
        self.btn_crop.setText(_translate("MainWindow", "确认开始转换"))

