# -*- coding:utf-8 -*-
# Author: maoge
# DATE  :2020/11/26
# TIME  :上午11:13
import sys
import os
import shutil
import cv2
import copy
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5.QtCore import *
from UI.PicConvertUI import Ui_MainWindow
from PIL import ImageQt, Image, ImageEnhance


# from system_hotkey import SystemHotkey


class PicConvert(QMainWindow,Ui_MainWindow):

    leftKey_signal = pyqtSignal(str)
    rightKey_signal = pyqtSignal(str)
    trueKey_signal = pyqtSignal(str)

    def __init__(self):

        super(PicConvert, self).__init__()
        self.setupUi(self)
        # pic rootpath
        self.ImageDir = None
        self.IMAGEIDX = 0
        self.imagelist = []
        self.all_orgImgPath = []
        self.each_classPathList = []
        self.pointIdx = None
        self.PreImg = None
        self.movedImgPath = None
        self.deletedList = []
        # self.hk_left, self.hk_right = SystemHotkey(), SystemHotkey()
        # self.hk_MoveTrue = SystemHotkey()
        self.setWindowTitle("picConvertV12.22b")
        self.pushButton_confirm.clicked.connect(self.getDirPath)
        self.pushButton_clearPath.clicked.connect(self.clearPath)
        self.btn_left.clicked.connect(self.PreImage)
        self.btn_right.clicked.connect(self.NextImage)
        self.btn_moveImg.clicked.connect(self.MoveImage)

        # add 3 button about 1~3
        self.btn_move1.clicked.connect(self.MovePad1)
        self.btn_move2.clicked.connect(self.MovePad2)
        self.btn_move3.clicked.connect(self.MovePad3)

        self.light_switch.stateChanged.connect(self.light_checkbox)
        self.LightScrollBar.sliderMoved.connect(self.test_ScrollBar)
        self.LightScrollBar.sliderMoved.connect(self.showLightImage)

        self.light_switch.setEnabled(False)
        # self.label_imgname.setWordWrap(True)
        # self.label_imgname.setAlignment(Qt.AlignTop)
        self.label_orgimgname.setHidden(True)

        if self.light_switch.isChecked():
            self.LightScrollBar.setEnabled(True)
        else:
            self.LightScrollBar.setEnabled(False)

        # self.light_switch.clicked.connect(self.test_checkbox)
        # add cancel action
        self.btn_cancel.clicked.connect(self.cancelAction)

        self.leftKey_signal.connect(self.PreImage)
        self.rightKey_signal.connect(self.NextImage)
        self.trueKey_signal.connect(self.MoveImage)

        # self.hk_left.register(['a'], callback=lambda x: self.send_left_key_event("left"))
        # self.hk_right.register(['d'], callback=lambda x: self.send_right_key_event("right"))
        # self.hk_MoveTrue.register(['t'], callback=lambda x: self.send_move_key_event("move"))

    def light_checkbox(self):
        if len(self.imagelist) == 0:
            return 0
        if self.light_switch.isChecked():
            self.LightScrollBar.setEnabled(True)
            self.showLightImage()
            # self.showImage()
        else:
            self.LightScrollBar.setEnabled(False)
            self.showImage()

    def test_ScrollBar(self):
        pass

    def showLightImage(self):
        img_org = Image.open(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).convert('RGBA')
        h, w = img_org.size
        rato = min(200 / h, 200 / w)
        self.label_imgshow.resize(h * rato, w * rato)

        enhance_value = float((self.LightScrollBar.value())/50 + 1.0)
        # img_org = Image.open(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX]))
        img_bright = ImageEnhance.Brightness(img_org)
        imgb = img_bright.enhance(enhance_value)
        imgshow = self.pil2_pixmap(imgb)
        imgshow_scaled = imgshow.scaled(self.label_imgshow.width(),
                                        self.label_imgshow.height(),
                                        Qt.IgnoreAspectRatio,
                                        Qt.SmoothTransformation)
        self.label_imgshow.setPixmap(imgshow_scaled)
        imagepath = self.imagelist[self.IMAGEIDX]
        # imagename = imagepath.split('/')[-1]
        imagename = os.path.basename(imagepath)
        self.textEdit_imgname.setText(imagename)

    def showImage(self):
        img_org = Image.open(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).convert('RGBA')
        h, w = img_org.size
        rato = min(200/h, 200/w)
        self.label_imgshow.resize(h * rato, w * rato)
        if not self.light_switch.isChecked():
            img_pix = self.pil2_pixmap(img_org)
            imgshow = img_pix.scaled(self.label_imgshow.width(),
                self.label_imgshow.height(),
                Qt.IgnoreAspectRatio,
                Qt.SmoothTransformation)
            self.label_imgshow.setPixmap(imgshow)
        else:
            enhance_value = float((self.LightScrollBar.value()) / 50 + 1.0)
            # img_org = Image.open(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).convert('RGBA')
            img_bright = ImageEnhance.Brightness(img_org)
            imgb = img_bright.enhance(enhance_value)
            imgshow = self.pil2_pixmap(imgb)
            imgshow_scaled = imgshow.scaled(self.label_imgshow.width(),
                                            self.label_imgshow.height(),
                                            Qt.IgnoreAspectRatio,
                                            Qt.SmoothTransformation)
            self.label_imgshow.setPixmap(imgshow_scaled)

        # self.label_imgname.setText(self.imagelist[self.IMAGEIDX])
        imagepath = self.imagelist[self.IMAGEIDX]
        # imagename = imagepath.split('/')[-1]
        imagename = os.path.basename(imagepath)
        # point_x,point_y = self.getimgPoint(imagename)
        imagename_copy =imagename
        # self.label_imgname(QtCore.Qt.AlignTop)
        # tempImgname = None
        # label bug
        # lines = len(imagename)//28
        # if lines == 1:
        #     tempImgname = imagename[0:28]+'\n'+imagename[28:]
        # elif lines == 2:
        #     tempImgname = imagename[0:28]+'\n'+imagename[28:]
        # elif lines > 2:
        #     tempImgname = imagename[0:28]+'\n'+imagename[28:]
        # else:
        #     tempImgname = imagename
        # imagename = tempImgname
        # self.label_imgname.setText(imagename)
        self.textEdit_imgname.setText(imagename)
        # orgimage = '_'.join(imagename.split('_')[:-1]) + '.png'
        orgimage = '_'.join(imagename_copy.split('_')[:-1]) + '.png'
        imgorg_pix_show = None
        class_show = None
        for each_classdir in self.each_classPathList:
            if os.path.exists(os.path.join(each_classdir, orgimage)):
                # windows or  linux ｜split(os.sep)[1]
                class_show = each_classdir.split(os.sep)[-1]
                # class_show = each_classdir.split('/')[-1]
                imgorg = QPixmap(os.path.join(each_classdir, orgimage)).scaled(
                    self.label_orgimgshow.width(),
                    self.label_orgimgshow.height(),
                    Qt.IgnoreAspectRatio,
                    Qt.SmoothTransformation)
                # imgorg = Image.open(os.path.join(each_classdir, orgimage))
                # imgorg = cv2.imread(os.path.join(each_classdir, orgimage))
                # drawedImg = cv2.rectangle(imgorg,
                #                           (int(point_x)-20, int(point_y)-20),
                #                           (int(point_x)+20, int(point_y)+20),
                #                           (255, 255, 255), thickness=1)
                # imgorg_pix = self.cvimg_to_qtimg(imgorg)
                # imgorg_pix = self.pil2_pixmap(imgorg)
                imgorg_pix_show = imgorg
                # imgorg_pix_show = imgorg_pix.scaled(
                #     self.label_orgimgshow.width(),
                #     self.label_orgimgshow.height(),
                #     Qt.IgnoreAspectRatio,
                #     Qt.SmoothTransformation)
            else:
                # self.label_orgimgshow.clear()
                pass
        try:
            if imgorg_pix_show:
                self.label_orgimgshow.setPixmap(imgorg_pix_show)
                self.label_class.setText(class_show)
            else:
                self.label_orgimgshow.clear()
            # self.label_imgname.setText(os.path.splitext(self.imagelist[self.IMAGEIDX])[1])
        except Exception as e:
            pass

    def getDirPath(self):
        self.light_switch.setEnabled(True)
        path = self.lineEdit_ImageDir.text()
        # 料号路径
        orgpath = self.lineEdit_orgPath.text()
        # 待加入更多判断
        if not os.path.exists(orgpath):
            QMessageBox.warning(self, '注意', '原图路径不存在')
            return 0
        for each_class in os.listdir(orgpath):
            if each_class == '.DS_Store':
                continue
            each_class_path = os.path.join(orgpath, each_class)

            self.each_classPathList.append(each_class_path)
            for each_imagefile in os.listdir(each_class_path):
                if each_imagefile == '.DS_Store':
                    continue
                each_imagefilePath = os.path.join(each_class_path,each_imagefile)
                self.all_orgImgPath.append(each_imagefilePath)

        if not os.path.isdir(path):
            QMessageBox.warning(self, '错误:', "路径{}为空或不是文件夹".format(path))
            return 0
        else:
            self.ImageDir = path
        # true path
        # self.truePath = os.path.join(self.ImageDir, 'true')
        # if not os.path.exists(self.truePath):
        #     os.mkdir(self.truePath)
        # 需要剔除不是图片的文件
        tmp_imagelist = []
        for imgename in os.listdir(self.ImageDir):
            if imgename.split('.')[-1] in ['png', 'bmp', 'BMP', 'PNG', 'jpg', 'JPEG']:
                img = Image.open(os.path.join(self.ImageDir, imgename))
                h, w = img.size
                if h == 40 and w == 40:
                    tmp_imagelist.append(os.path.join(self.ImageDir, imgename))
                else:
                    pass
            else:
                pass
        if len(tmp_imagelist) > 0:
            self.imagelist = sorted(tmp_imagelist)

            # size_img = Image.open(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX]))
            # size_img = size_img.convert('RGBA')
            # imgshow = QPixmap(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).scaled(
            #     self.label_imgshow.width(),
            #     self.label_imgshow.height(),
            #     Qt.IgnoreAspectRatio,
            #     Qt.SmoothTransformation)
            # self.label_imgshow.setPixmap(imgshow)

            self.showImage()
            self.lineEdit_ImageDir.setEnabled(False)
            self.lineEdit_orgPath.setEnabled(False)
            self.pushButton_confirm.setEnabled(False)

        else:
            QMessageBox.warning(self, '注意', '当前路径下没有规范格式的图片')
            return 0

    # 热键信号发送函数(将外部信号，转化成qt信号)
    # def send_left_key_event(self, i_str):
    #     self.leftKey_signal.emit(i_str)
    #
    # def send_right_key_event(self,i_str):
    #     self.rightKey_signal.emit(i_str)
    #
    # def send_move_key_event(self,i_str):
    #     self.trueKey_signal.emit(i_str)

    def NextImage(self):
        self.deletedList = []
        if self.IMAGEIDX + 1 < len(self.imagelist):
            self.IMAGEIDX += 1
            # imgshow = QPixmap(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).scaled(
            #     self.label_imgshow.width(),
            #     self.label_imgshow.height())
            # self.label_imgshow.setPixmap(imgshow)
            self.showImage()
        else:
            QMessageBox.warning(self, '注意', '图片到底了')
            # self.textEdit_imgname.clear()
            return 0

    def showNext(self):
        # self.deletedList = []
        if self.IMAGEIDX + 1 < len(self.imagelist):
            self.showImage()
        else:
            QMessageBox.warning(self, '注意', '图片到底了')
            self.textEdit_imgname.clear()
            return 0

    def PreImage(self):
        self.deletedList = []
        if self.IMAGEIDX - 1 >= 0:
            self.IMAGEIDX -= 1
            # imgshow = QPixmap(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).scaled(
            #     self.label_imgshow.width(),
            #     self.label_imgshow.height(),
            #     Qt.IgnoreAspectRatio,
            #     Qt.SmoothTransformation)
            # self.label_imgshow.setPixmap(imgshow)
            self.showImage()
        else:
            QMessageBox.warning(self, '注意', '当前文件夹开头了')
            # self.textEdit_imgname.clear()
            return 0

    def MoveImage(self):
        # 移动后，list去掉对应元素，idx不变
        self.truePath = os.path.join(self.ImageDir, 'true')
        if not os.path.exists(self.truePath):
            os.mkdir(self.truePath)
        try:
            shutil.move(self.imagelist[self.IMAGEIDX], self.truePath)
            self.imagelist.remove(self.imagelist[self.IMAGEIDX])
            if not os.path.exists(self.imagelist[self.IMAGEIDX]):
                QMessageBox.warning(self,'注意', '图片不存在或者分图完毕')
                return 0
            # imgshow = QPixmap(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).scaled(
            #     self.label_imgshow.width(),
            #     self.label_imgshow.height(),
            #     Qt.IgnoreAspectRatio,
            #     Qt.SmoothTransformation)
            # self.label_imgshow.setPixmap(imgshow)
            self.showImage()
        except Exception as e:
            self.label_imgshow.clear()
            QMessageBox.warning(self,'注意:','图片路径问题或者是路径下没有图片')
            return 0

    def MovePad1(self):
        self.pad1Path = os.path.join(self.ImageDir, 'jicai_zawu1')
        if not os.path.exists(self.pad1Path):
            os.mkdir(self.pad1Path)
        try:
            # 删除前的路径
            # self.deletedList.append(self.imagelist[self.IMAGEIDX])
            # deepcopy
            deletepic = copy.deepcopy(self.imagelist[self.IMAGEIDX])
            self.deletedList.append(deletepic)
            # print(self.imagelist[self.IMAGEIDX])
            # 移动后的文件图片路径
            self.movedImgPath = os.path.join(self.pad1Path,
                                             os.path.basename(self.imagelist[self.IMAGEIDX]))

            shutil.move(self.imagelist[self.IMAGEIDX], self.pad1Path)
            self.imagelist.remove(self.imagelist[self.IMAGEIDX])
            if not os.path.exists(self.imagelist[self.IMAGEIDX]):
                QMessageBox.warning(self,'注意', '图片不存在或者分图完毕')
                return 0
            # imgshow = QPixmap(os.path.join(self.ImageDir, self.imagelist[self.IMAGEIDX])).scaled(
            #     self.label_imgshow.width(),
            #     self.label_imgshow.height(),
            #     Qt.IgnoreAspectRatio,
            #     Qt.SmoothTransformation)
            # self.label_imgshow.setPixmap(imgshow)
            self.showNext()
        except Exception as e:
            self.label_imgshow.clear()
            QMessageBox.warning(self,'注意:','图片路径问题或者是路径下没有图片')
            return 0

    def MovePad2(self):
        self.pad2Path = os.path.join(self.ImageDir, 'jicai_zawu2')
        if not os.path.exists(self.pad2Path):
            os.mkdir(self.pad2Path)
        try:
            # 临时备份删除图片以用于撤销
            deletepic = copy.deepcopy(self.imagelist[self.IMAGEIDX])
            self.deletedList.append(deletepic)
            self.movedImgPath = os.path.join(self.pad2Path,
                                             os.path.basename(self.imagelist[self.IMAGEIDX]))

            shutil.move(self.imagelist[self.IMAGEIDX], self.pad2Path)
            self.imagelist.remove(self.imagelist[self.IMAGEIDX])
            if not os.path.exists(self.imagelist[self.IMAGEIDX]):
                QMessageBox.warning(self,'注意', '图片不存在或者分图完毕')
                return 0
            self.showNext()
        except Exception as e:
            self.label_imgshow.clear()
            QMessageBox.warning(self,'注意:' ,'图片路径问题或者是路径下没有图片')
            return 0

    def MovePad3(self):
        self.pad3Path = os.path.join(self.ImageDir, 'jicai_zawu3')
        if not os.path.exists(self.pad3Path):
            os.mkdir(self.pad3Path)
        try:
            deletepic = copy.deepcopy(self.imagelist[self.IMAGEIDX])
            self.deletedList.append(deletepic)
            # 移动后的文件图片路径
            self.movedImgPath = os.path.join(self.pad3Path,
                                             os.path.basename(self.imagelist[self.IMAGEIDX]))

            shutil.move(self.imagelist[self.IMAGEIDX], self.pad3Path)
            self.imagelist.remove(self.imagelist[self.IMAGEIDX])
            if not os.path.exists(self.imagelist[self.IMAGEIDX]):
                QMessageBox.warning(self,'注意', '图片不存在或者分图完毕')
                return 0
            self.showNext()
        except Exception as e:
            self.label_imgshow.clear()
            QMessageBox.warning(self,'注意:','图片路径问题或者是路径下没有图片')
            return 0

    def clearPath(self):
        self.ImageDir = None
        self.IMAGEIDX = 0
        self.imagelist = []
        self.lineEdit_ImageDir.setEnabled(True)
        self.lineEdit_orgPath.setEnabled(True)
        self.lineEdit_ImageDir.clear()
        self.pushButton_confirm.setEnabled(True)
        self.label_imgshow.clear()

    def pil2_pixmap(self, pil_img):
        pixmap = ImageQt.toqpixmap(pil_img)
        return pixmap

    def pixmap2_pil(self, pixmap):
        img_obj = ImageQt.fromqpixmap(pixmap)
        return img_obj

    def getimgPoint(self, imgnameWithsuffix):
        imagename = os.path.splitext(imgnameWithsuffix)[0]
        pointIdx = imagename.split('_')[-1]
        pointIdx = int(pointIdx) * 2
        points = imgnameWithsuffix.split('_')[-2]
        points_list = points.split('-')[3:]
        x, y = points_list[int(pointIdx) - 2], points_list[int(pointIdx) - 1]
        return x, y

    def cvimg_to_qtimg(self, cvimg):
        height, width, depth = cvimg.shape
        cvimg = cv2.cvtColor(cvimg, cv2.COLOR_BGR2RGB)
        cvimg = QImage(cvimg.data, width, height, width * depth, QImage.Format_RGB888)
        piximage = QPixmap(cvimg)
        return piximage

    def cancelAction(self):
        # 两个名字相等才能撤销成功
        if len(self.deletedList) == 0:
            # QMessageBox.warning(self, "Warning", "can't use cancel operation")
            return 0

        if os.path.basename(self.deletedList[-1]) == os.path.basename(self.movedImgPath):
            # self.imagelist.append(self.deletedList[-1])
            self.imagelist.insert(self.IMAGEIDX, self.deletedList[-1])
            try:
                shutil.move(self.movedImgPath, os.path.dirname(self.deletedList[-1]))
            except Exception as e:
                QMessageBox.warning(self, "warning", "File already exists")
                return 0
            self.showLightImage()
            self.deletedList = []
        else:
            QMessageBox.warning(self, "Warning", "cancel error")


def main():
    app = QApplication(sys.argv)
    picConvert= PicConvert()
    picConvert.show()
    picConvert.raise_()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()